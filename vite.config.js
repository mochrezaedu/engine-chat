import { defineConfig,loadEnv } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig(({mode}) => {
  Object.assign(process.env, loadEnv(mode, process.cwd()));

  return {
    build: {
      rollupOptions: {
        output: {
          entryFileNames: `assets/[name].js`,
          assetFileNames: 'assets/[name][extname]'
        }
      },
    },
    plugins: [react()],
    ...process.env,
  }
})
