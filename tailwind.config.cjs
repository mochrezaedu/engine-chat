/** @type {import('tailwindcss').Config} */ 
module.exports = {
  important: '.upscale-App-Root',
  prefix: 'upscale-engine-',
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  }
}