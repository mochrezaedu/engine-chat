import './preflight.css'
import { useEffect, useMemo, useRef, useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import ChatList from './components/ChatList'
import InputBase from './components/InputBase'
import InputSelect from './components/InputSelect'
import LabelText from './components/LabelText'
import LabelSelectMultiple from './components/LabelSelectMultiple'
import axios from 'axios'
import {useAtom} from 'jotai'
import { 
  flowGroups as derivedFlowGroups, 
  flows as derivedFlows,
  conversations as derivedConversations,
  isTyping as derivedIsTyping,
  inputBase as derivedInputBase,
  inputSelect as derivedInputMultiSelect,
  appendConversations,
  user as derivedUser,
  assistant as derivedAssistant,
  editConversation as derivedEditConversation,
  busy as derivedBusy,
  userToken,
  env,
  anonymous,
  user,
  conversations,
} from './services/stores'
import { useCsSession, useInputManager, useStorage } from './services/hooks'
import { objectToCamelCase } from './helper'
import EditConversation from './components/EditConversation'
import { CSSTransition } from 'react-transition-group'

function App() {
  const bodyReff = useRef()

  const [assistant] = useAtom(derivedAssistant)
  const [flowGroups, setFlowGroups] = useAtom(derivedFlowGroups)
  const [flows,setFlows] = useAtom(derivedFlows)
  const [conversations, setConversations] = useAtom(derivedConversations)
  const [editConversation,setEditConversation] = useAtom(derivedEditConversation)
  const [isTyping,setIsTyping] = useAtom(derivedIsTyping)
  const [input,setInputBase] = useAtom(derivedInputBase)
  const [inputMultiSelect,setInputMultiSelect] = useAtom(derivedInputMultiSelect)
  const {resetInputs} = useInputManager()
  const [busy] = useAtom(derivedBusy)
  const [open,setOpen] = useState(false)
  const [maximize,setMaximize] = useState(false)
  const _tr_ = useRef(null)

  const csSession = useCsSession()

  const lastConversation = useMemo(() => conversations[conversations.length-1], [conversations])

  const _getFlowItem = () => {
    return (lastConversation ? lastConversation.flow: flows[0])
  }
  const _getNextFlowItem = () => {
    return flows.find((item) => (item.id == _getFlowItem().next || item.name == _getFlowItem().next))
  }
  const _storeFirstConversation = () => {
    setIsTyping(true)
    axios.post('/conversation/store_empty', {}).then((_res) => {
      if(!_res.data.error) {
        setTimeout(() => {
          setIsTyping(false)
        }, 500)
        appendConversations({
          payload: _res.data,
          state: conversations,
          setState: setConversations
        })
      }
    }).catch((_err) => {
      console.log(_err)
      alert('Error while store Conversation')
      setTimeout(() => {
        setIsTyping(false)
      }, 500)
    }).finally(() => {

    })
  }
  const _handleSubmitInput = () => {
    const {value} = input
    if(value != '' && value?.length > 0) {
      resetInputs()
      axios.post('/conversation/store', {
        "flow": {id: lastConversation.flow.id}, "next": lastConversation.flow.next, "type": "sender", "text": value, 'is_free': (busy ? 'no':'yes')
      }).then((_res) => {
        appendConversations({
          payload: _res.data,
          state: conversations,
          setState: setConversations
        })
      }).catch((_err) => {
        alert('Error while store Conversation')
      })
    }
  }
  const _handleSubmitInputMultiple = (args) => {
    const storeInputMultiSelect = inputMultiSelect
    resetInputs()
    if(inputMultiSelect.selected.length > 0) {
      const userInputs = inputMultiSelect.selected
      const inputPayload = userInputs.map((item) => item['text'])
      resetInputs()
      axios.post('/conversation/store', {
        "flow": {id: lastConversation.flow.id}, 
        "next": lastConversation.flow.next, 
        "type": "sender", 
        "text": '',
        "texts": inputPayload,
        "fetch_type": 'multiple'
      }).then((_res) => {
        if(_res.data) {
          appendConversations({
            payload: _res.data,
            state: conversations,
            setState: setConversations
          })
        }
      }).catch((_err) => {
        alert('Error while store Conversation')
        setInput({...input,show: false})
        setInputMultiSelect({...storeInputMultiSelect})
      })
    }
  }
  const _scrollToDownBody = () => {
    bodyReff.current.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }

  useEffect(() => {
    // PROFILING
    axios.post('conversation/get_recent_messages', {}).then((_res) => {
      const {data} = _res
      if(data.data.length == 0) {
        _storeFirstConversation()
        setIsTyping(false)
      } else {
        setConversations([...data.data])
      }
    }).catch((err) => {
      alert('Error while fetching messages.')
      console.log(err)
    })
  }, [])

  useEffect(() => {
    _scrollToDownBody()

  }, [lastConversation])

  return (
    <CSSTransition onEnter={() => console.log('fired')} nodeRef={_tr_} 
    in={maximize} timeout={200} classNames={'upscale-engine-window'}>
    <div style={{transition: 'all 300ms ease-in-out',zIndex: 9999999}} className={`upscale-engine-rounded-md ${maximize ? 'upscale-engine-bottom-[0px] upscale-engine-right-[0px] upscale-engine-rounded-none': (open ? 'upscale-engine-bottom-[0px] upscale-engine-left-[0px] md:upscale-engine-left-auto md:upscale-engine-right-[1rem] md:upscale-engine-bottom-[1rem]': 'upscale-engine-left-auto upscale-engine-right-[1rem] upscale-engine-bottom-[1rem]')} upscale-engine-fixed`}>
      <div ref={_tr_} className={`${open ? 'upscale-engine-block':'upscale-engine-hidden'} upscale-engine-window`}>
        {
          <div className='upscale-engine-relative upscale-engine-border-gray-100 upscale-engine-border-solid upscale-engine-bg-white upscale-engine-rounded-md upscale-engine-flex upscale-engine-flex-col upscale-engine-shadow-md upscale-engine-w-full upscale-engine-h-full'>
            <div>
              <div className='upscale-engine-relative upscale-engine-bg-white upscale-engine-flex upscale-engine-justify-between upscale-engine-items-center upscale-engine-p-3 upscale-engine-pb-2 upscale-engine-border-0 upscale-engine-border-solid upscale-engine-border-b upscale-engine-border-b-gray-300 upscale-engine-z-20'>  
                <div className='upscale-engine-flex'>
                  <img className="upscale-engine-self-center upscale-engine-mr-2 upscale-engine-p-1 upscale-engine-w-9 upscale-engine-h-9 upscale-engine-rounded-full upscale-engine-ring-2 upscale-engine-ring-gray-300 dark:upscale-engine-ring-gray-500" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA4VBMVEX////U1NSzs7NvwL/Z2dnq6uozMzPw8PCjo6NmZmZNTU2/v7/Pz8/S0tLW1taxsbEuLi4rKyve3t4kJCTI0tKnu7q4uLhkvLtYWFgZGRnIyMhvwcCmpqaRkZHl5eW2srI7OztGRkbl8/N4eHiamppsbGyKiop+fn5ycnJOTk6CyMeFychBQUF7e3vS6urw+Pim19apy8rJ5uaKv764394vJSaIs7KaqKiW0M+cvLugycmny8qiu7tNc3JosK93vLu7z86Lm5tZjo0/UlI6RUVVhYRhoKBKbW2EtbQUFBSUrawgQhYYAAAUAElEQVR4nN2dCXfbNhKAw8s6KPG0KZmkKMm6bdlOa3sTZ1O727TN7v7/H7QAwRsACZCUJe28vtfESUh8nMHMABgAnz4dTpSB6k/Wi+2q19/txr1FV1Q7B3zdB4usr3vj8biHpB/KfL+fzTdd69hta0Hk5TZmyxIizPl+vDxzyMEij5cnRMpcDY7dyvoi43wYIWCcbYNjt7Sm6AQ+AiG0VvHYba0lEyIgibDfn3WP3doaQgEkE/Zn/rHbyy0uBZBC2N+fnU+l8FEJ55tjt5hTZJoKaYT9/bGbzCkqN+HszMz0/59Q4Cc8s0y8w03YO3aTeYUGSPWlk2O3mFcmnIT7s0vcRF5C99gt5pKOJfDmNGNdd5Vjt5tVFG0oCNqWi3C+dURdV88iYgSQTxDkJR/hUoSiOyfPqCA+oEOVi3AnRnLijJ2YDyJuOAjnG0dMGU+2P3aslA8QUrwpmVAXM6KfqF8NhLxoC2bC+cIRc4jiCZpq1kDjnsg6T9Pf5VQYMqrHBiqKUuSD7pSY15BUOHGKhCB0nNYEnEUAFLQhKSYSAFc4IGQ8oXnUjkbgg4guC+F8TOI7KUvNWahs5bpiNeF8jHXC1FJPY8wYZAC1oNORc4irCsJ5jwoIGU+hM2YAh1YHSFaJgjbYjMsI94sSPogoH5sv62M0BfApWRXCH2o6nXA+XhKdTBZROx1AOVQg7lQ1wd+SCOfzMSFK4IjDUwEMoAKJTlWTBWeyyBLOgYzvuw4DIEAUTgIwtFCLxBdDumJ3sr5frPq7ce9+s/RFJrwjG2oGEFqoTAVEHVKTgQiiA4WVDiEey92kXhR2QaWUL+EccLHFiMcJGmmgh0GiOLKgSS1CUf/9CIDPOUB6F2yFUNRvbz6Y7+F9kAMkZd5tEk6vRp8/FPB5dFkPsC6hOP0++kA1Pt6Nfokjn8xjog0IxenTSPr1gwAfRqOnuoD1CUVR8kZfPwTweSRJsY1qfCbaiHB65UkfYamPX0eSl9goyGQCLsAmOpw+eZIkPRwa0AOAdxHgMGAN9K0QAjsFMno+KOBN+I7YRqEb5QRsRDj9DpV40LDxK+iCkvctUiHshJQZmgPpENnpAf0N9DFAoo43VDjdaAuEV15oRHcHBUxCocWejLZFGCkRuNQDAnrvUVNr2WhTQtEJCSXp9rF9wM/IRJNIEdSx0caEkRIPgRj1QUlKbZQ0k39oQvEqIpTaNtQYMOmFSi0bbU6YKLFlxESDnpCosIabaYEwVaLUpkd9iAGlb0nKHdRSYXPC6XuM2GJcvEkA02FhXWlO+JoosbXs5jF+YpqRHpFQnCaEreWot+kTfzkFwqcMYisjja+JjabjwmMSZnwNkBbGi88poPfUWIVtEIrvGcLmMeMho0Hv95MgzJppc4f6mPlcklcniTmADnNm2tTbfM0C3jZXYSuEopSTRl0x0wkB4T9PhDBnps264k0WsIVw3xbha46wSeC/lXLSAmA7VprriE2iYs5G20ho2iJ0pILUBMzbaJJ1K0H5UugHEKbZdzM7vcsDjlA01OqOm9okzLsa0LZa/vTXggojR2NVLWd/BOFrgbCWP32UioLifcA/C9w6YdHVgM9fY1nqc0GF3i1qotLpNElt2iF0ioSSxw14gz0iSrubdcOWCEWMkN/ZfC0+InKlWr1JxLYJ3zFE3uStGCmSnE0OipVrxyDEnCkQzkHGHfaAZHzfbIBxOEK+iPGAqRCEw0ZkhyfkUyKuQkk6KUIsIHIqkaBCyWtjZNEa4XcCIY8ScUd6FoTsSsQdKZQzIGSOiZ9JgKdFiKVtSBiX3B6JKjwHQtZZqeezJWQdYpD/8VkQss1nkELFuRCyBQxSqEgJNSRyTqIftkzoFIWBcMTga/CRLxLvEqBY8sBVHd3vLifr9XoDBPxvsvR1XXTcIfjjELcJIWLR/eVyMllv7heLLZL7zXoyWS67PvobVEKGkXBx8gKwhfL2Zbnp9WcXhmHaQMyMoN8bF7P+eLvu6q4W6ZWHECmpO1lvx7t9+BaSGMYFeEtvu/7XW9goHJFh6btopOAxb3/8/PPlBVDAF5QJbBfEnY03E90dkPRJInRE3Z/c9/oX4YeregmifzF+/Pb3GwGy0kwfvQLf22+ArvKtxUZA0H1v7WiyVkHo6JPVbmYzkGHveHn58ddbgbE6JOaN1Hv7Ac2vpgDO+dq1YsYhEGGgI4ltc9mbcbNl3vBi/CblESvNNGek3t8vtV+eQPb0SJHZ7ZKuHqpvPa///SJ5+fMth+hVmWneRi+I7w9t8Nq2jeS315GzIf11w+4vXcCY3w8K3cr2gsgX92bY8dFPzOh3xNa8/Myv01R409ywwpNe8Aeattm7n+iqOxhGUVETBgPX0ZfrRa9vXtt4qw37YuUXThEQNzvTxB8OPtbFrrdYgwDkqKobi6rC56/6yVfNPv1H3kwrBhiFxRiM0LheqAHcp9bpKBnpRKIEmjjpAZUWGw+0kD9EYFxsK7SE2Qq4YEG2kOSSCvSTQXdnY036WfCn5YT59TTvz0IzzLGcR8MFggr+dlaEvC4Q5p9smvuFPwyKXJhYgY7p/a+CNy0dBxcSGu9nvh3GLqjgSzADZ2HkPncJoWHvN45cBRczqgUtvvzNEy8KWbf3R95MbZUNEEFaEyOjRzqhPe8KjHgh4iZvHcZbwUpLs+/C0NB7yxEafWY+BGn10u9NJbTXHHhQCkr8UyrmNWWE2Lgi1xHNe3YVRow9s4rQXvLxAd+9zxnpb9g6VFlHLCzcw5wmo0W7y0uoBLMKQnPFCyhbvfSzGy8/MRWWDYPxkZPn/ZFGfdvhJux07QpClZ/wPjEM80cxL5XKXQ1xJvgpNagBL6CiWOU6NMbcgLI1iQnNFWENqtTVEOagvCfViS3NDvgJO7FJkQn5eyEg9CO7MHZ+l4RYMh+FT2B47yB3WkXfzOQ2UkAYf3AKoVOD0IkIzbXf/UJSIj35xhdkvCtVVbtNCOMPTia8drkB03Ax6wL5B9c6G746/gQAVTcy01kdQrGcUKhPaPR8iIirkD68wCe7QxWq7hYpcX8qhGZipED+zeFMCSsyaiixy29kpfmjH1qw0tkSApJ6InUAhQULZKRAGvTDZUT4n/wIuB95GrU2oTEOVUgyU2q4wCYSvdeIcGXUJozyZHOVO9siiAOQX9uXmpuIEA8Y1HCBhUPUDYEgRdhWDcIoHtqiJsNwGoSz4wMn6tr2pAZh1GmQkZK8KbV8CFs3HEWAqoiaM+BXYjA3UHCGU6fpXJsYdU9zWzenMfqRCrv/xQlpAREj9GJCNXQMNfJSRbCj3EXOz5c6yNUY+xqEC0hoLmJC3NVQVy/wlCYhXJthM/kz79iVulqBcBIFtRrOFH3u2Ei7X7BmU0M+ltK8J4S+EXoLfkLU38ytjM15o1Ge3eVWooBihV+DEBsdpoQqbI8x5wVUlMhIVa1I6KAxkNHjJgyDRZTQUAhpI0SsG6aELrJ93uFTR49Cl4WvW/izemZqhfYdJTTtEaK0xvY5zbSD/Imtazihg4Ys9ppTiWiIP+umghPSEtMywtA0eDtix41UmC5AZdaefBQSZ1o1VU7sbEJD1iErYcbTqG745Uy+mB+F+4wKs4SxEvmCPhr/mvetE6phmOUz02hcYfYyFanZ9UM9SneGXIToqy27rRBKGUIxTCTGPITBHqWzqkYmdDYmf14zCO1+7mcI8YhPI8SznwwhSmuuhxwqXKBsdpMtKs6vAaOUjif9jjzpopyQ5kuL8TDNS2FHDLNvc8OsxI5/jfKy3EJ3nnAZzbixTylq4UcxM4CEvJSdMKdDNJdhyoyInWicajq5lfw8obNAYX/POtRH4wpj7NQjxLK2ZPQERQ5zU7PHCDhAM8n2JF/4XqhUcHaoq47ZQoblRnm8mCHER0/UrA3LvL3vGUJhgJ6+Uhi0GAOaq0Jlf7EWQ5/FiAyGarnh3wYqzBISJmrYR0+vGcKhjEae9k6rRAQmGje8otrE6aKxh9l3KxEtP1rJ98Us4Xux2fTxIT7Gf8oSCjIK0YaxLF8o7QTR0MjsD4olQ3g9TextLvzyVTbL3UZPXTtVhBRAQsHXe45QG+yiuYd9N6CtdoOf+/uoKTuXoSbKWUa1DXZPp65yW5Z7fxG9fAtL+MoCPn2ehrAwkyMEiH30FsO2t6KlZAsWolIFS1+YdtxiAS9tI1R9Od1ZpEZ7ttHjQoWUDfzaXe7iugCzF9YolrlSetUQYSvQVY5Q0IbJqq5pX4/vl447RIWXsN5ksunNkjIMw14XK74ohKKjj9On2vPVZgJrTaI6E7G7Xu2v09qd6KKIMldKn00kzHm/5gkFTcsszsPKoWuAZIf/y1T4QAXOdeL+KEpt4iatBkoLhuyo7DF9qjmLD8gu64YlC4h42vZeIBQ02d1eV5VqGbY5EcgbwCiEjt8jFAMVnmoai+QE6ZSQsDJDL4vCkppsVhPv6AKM675hU8vtYL3eZEiy0BJCyLia0b8cUOzFbp05IbusG5YscxPWD18xQsgoq8vevFjJhszrYrwRsZpLBsKwDrM3N8KH5p4KPpkx76393BnZJUZathWRtA+BQIggB6q/XoznM9AHYS+82I97m3XXcUvwyglhMabuw/I48FRUiWwbs/kYeh6xeAQ4fWBRumMWD4gZJWL3yMB6dlTrPBgkJ5JX1bNXV7InNetRESr5fPNEhXyV0KQNTx6NMEPKWqjPRMgm9F5YWp1IqtNP3GkLh9O0TkjIZyoqTEk7K+Pk9AQJiYDlNVHFTfhZLZ4e4X+JgOV1beQdQV7obk6N8As+LkRSWstO3TAjPb2eFuE/3kn7SUIjLd+PQPlXUI9nsbNLqty/Rt4+GsrZEJYCUrfmnRFh1Qa9syes3BZEiojnRVi1ZZ2yCfh8CCu3AlM2cp8NIcOefKqZnglh9bkKVDOlEA7h4IL9SBdWQlhVBMdN5Jv16IQM+9Wx45PKCbXwattOJ5BhxVM7hIDOceXwIj5F4CNkOnOAZqZEwuyFYZ3AqqasIkR0yVaxTsfiI2Q5/IMw0KcSDouXTHaUCsoSQljv5gzkTkE0gqFSCdkOVeDQIfnaXqBMWRuSQYmEIRuAs4p0oRAmMahnKrAd20bJTQmEmAqzmECdshZu/h0SCPVE4E5K2QqIcDQl0ggZT20jH09DImS5tg+ubASBZcWTVuFME9weOhgMNCtQFDpaJAE7IesBNWRfU5MQJ+YVhZmQ+ZAh8gCDZKUfQmgxE7IfFIXP7lMIa9zXy0/oshJynEVLDBgkX1rDTLkBFYLzJRPyHPFJ+vfEeMh/uSQ3ocpKyHWcMCk5JeY0Q+XQhAQbpRDyndKK/3vKTBS3FtsAJJ5mxnkiNOGUGsqpgkNyXtMOoeJQxhaEUwV5L2XBK8Cod1toXB6VB1Cm3UpOOBmS+1Bvwh4o6vISlxrZ+QKVfu063ov470jAE5uyIQN7b2Q2UHIPRII7mhrnXWPZ6aj0dgtmPTLqz9VLAHEjrXVmOXYYdMWp80OLqT+y8MlqGR/JSCvP3SHJI/adquaihlpQDVmJZ7kUB5qqEPOkdY7z/oRHDJarA6ohK/AGToX6oGD1F3Wv7yoWn5T3xARSKIWk0ymWqzPgEVTo1b2mpLiamNx9WAk5lKmUZLrAclUmPCiYjda/agaz02+sJRfhPCosXqwmVCxNdURWOvzqh2bXsHwtILLZaUaXIOOBUzBkwkAbuo7IrDqajbKcBkkVzJ/WWgkOJ6M0WCkaAGtERZWOmD2ZjgMQy7lr+tFYismbV5rZMMigHlgCiN/70PRKK2ykeNuQsC4bAiTE+oaAhHrF90ZabOM+4JwKW7haDnvo+2WDS4Ma3VpNAGzWCZFgKbgnNbhhrsnd6viwt6VbLAnTp/XvJ6tNOCWNetu6iRSflxrd1bXUuoTTK0IlaXsX5pIK+mpe2FmPcCq+koq8WrzXuZjb1FdjLUKiAlu5obMU0Rs91WCsQTh1nohVeq0Cfvp0R6w8/cbNyE04Fcl8bQTCvBBXazyJl5GPcDq9IvO1dE9uXohahCP/35nL2DkJp9PvtCrSQwAS+2KIOAKKJOxSa0g4FaH6aGUzBwGkIsJPegshWSjZCAHeK7UIWGrdybAggta8P/0+rN53UU04nTrfn8rwDghYVrmIMG+/xfsR6xI6V5CuDG9022Kgx4VWThQzjr50fd1xB0OBspOmlFDX/S7prqocYFu5KE0eyt/vRUcB+GgX6GCoFYSwDzgsrvF9v2SfTwawndFEmTzelqlxJGRPO4Ckvi46any8Ojy8He3cisqFfD9BK9vJlD6/jfFgpRA31sQ6HGpqt5GUEtae+uWU55I2gA7o+tUcdCEeuRop8OtBfUxWbuiWCpyJJjRRo1+/Cr9VoVkqmonThk41Cifh6PaDLDSWB6Ia4wtngR6dmrZKIfwAH4oJKfp7T0ng0wRXb43wwxWIhNAbc+UMIPipNSBJ1U4f2gOz8qtUZCwUbICkxuU1V5zv41woQYr1xPjyG8xjVJGDsrDAO7o70EiJVW7y4w3iAmOYrbmqyGSyfm5adOR9SBLDzujRp4rDfSchZ5k+dVW7806LD0rKSC8NS7UJZAgXEOFdZKnouqiqA/iHTwmfdCJ8UG4+eyEk6xpxccQRC/yjb3H/OyE+KI/PkLGFi59DwtHo65H9C1Eevo4qi4oYCP85GknPx4wPZfL4fNl8T/svJ6m+VDpWk8vXhwNZOVX1ZeQxuGTaqFekG15a3FXjxxPF4qIcArzgKMl1E3nshDvYGODkszBNijx2AssSLgfD4mY9+IPLS9mylM750mXl8eYmCBQFXo5ngV+A/26OAPY/+MsCEVQQ8nsAAAAASUVORK5CYII=" alt="Bordered avatar" />
                  <div className='upscale-engine-text-lg upscale-engine-font-bold upscale-engine-text-gray-700'>
                    <div>{assistant.long_name}</div>
                    <div className='upscale-engine-text-xs upscale-engine-text-indigo-700 upscale-engine-text-left'>Online</div>
                  </div>
                </div>
                
                <div className='upscale-engine-flex upscale-engine-items-center'>
                  <button onClick={() => setMaximize(!maximize)} className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-justify-center upscale-engine-items-center upscale-engine-mr-1 upscale-engine-hidden md:upscale-engine-flex'>
                    {
                      !maximize &&
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 15.75l7.5-7.5 7.5 7.5" />
                      </svg>
                    }
                    {
                      maximize &&
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                      </svg>
                    }
                  </button>
                  <button onClick={() => {
                    setOpen(false)
                    setMaximize(false)
                  }} className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center'>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 19.5l15-15m-15 0l15 15" />
                    </svg>
                  </button>
                </div>
              </div>
              {
                !csSession.isIdle() &&
                <div className={`upscale-engine-relative upscale-slide-down-animation upscale-engine-w-full upscale-engine-text-center upscale-engine-text-sm upscale-engine-py-1 upscale-engine-z-10 ${csSession.isConnecting() && 'upscale-engine-bg-gray-200'} ${csSession.isConnected() && 'upscale-engine-bg-green-600 upscale-engine-text-white'}`}>
                  {
                    csSession.isConnected() ? '[Testing] Tersambung': '[Testing] Menyambungkan ke CS ...' 
                  }
                </div>
              }
            </div>
            
            <div ref={bodyReff} className={`upscale-engine-flex-grow upscale-engine-p-3 upscale-engine-flex upscale-engine-flex-col-reverse upscale-engine-overflow-y-hidden hover:upscale-engine-overflow-y-scroll upscale-y ${Boolean(input.status === 'enabled' && busy && input.label) ? 'upscale-engine-pb-8': ''}`}>
              <ChatList />
            </div>
            <div className={`upscale-engine-flex upscale-engine-justify-between upscale-engine-p-2 upscale-engine-border-t upscale-engine-border-b upscale-engine-border-gray-300 upscale-engine-relative ${Boolean((input.status === 'enabled' || inputMultiSelect.input.status === 'enabled')) ? '': (busy ? 'upscale-engine-bg-gray-200': '')}`}>
              {
                inputMultiSelect.show &&
                <InputSelect onKeyUp={(e) => {
                  if(e.key === 'Enter' || e.keyCode === 13) {
                    _handleSubmitInputMultiple(e)
                  }
                }} onSubmit={_handleSubmitInputMultiple} />
              }
              {
                input.show &&
                <InputBase withLabel onSubmit={_handleSubmitInput} disabled={Boolean(input.status === 'disabled' && busy)} onKeyUp={(e, v) => {
                  if(input.value != '') {
                    if(e.key == 'Enter' && e.keyCode == 13) {
                      _handleSubmitInput()
                    }
                  }
                }} onChange={(e) => {
                  setInputBase({...input, value: e.target.value})
                }} />
              }
            </div>
            <div className='upscale-engine-rounded-b-md upscale-engine-text-gray-400 upscale-engine-px-3 upscale-engine-py-2' style={{fontSize: '0.66rem'}}>Powered By <span className='upscale-engine-text-indigo-700'>upscale.id</span></div>
          </div>
        }
        {
          <EditConversation className={editConversation.show ? '':'upscale-engine-hidden'} />
        }
      </div>
      <div onClick={() => setOpen(!open)} className={`upscale-engine-cursor-pointer upscale-engine-rounded-full upscale-engine-w-9 upscale-engine-h-9 sm:upscale-engine-w-12 sm:upscale-engine-h-12 upscale-engine-bg-gray-700 upscale-engine-text-white upscale-engine-ml-auto upscale-engine-mt-4 ${open ? 'upscale-engine-hidden md:upscale-engine-flex':'upscale-engine-flex'} upscale-engine-items-center upscale-engine-justify-center`}>
        {
          !open &&
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-5 upscale-engine-h-5 md:upscale-engine-w-7 md:upscale-engine-h-7">
            <path strokeLinecap="round" strokeLinejoin="round" d="M12 20.25c4.97 0 9-3.694 9-8.25s-4.03-8.25-9-8.25S3 7.444 3 12c0 2.104.859 4.023 2.273 5.48.432.447.74 1.04.586 1.641a4.483 4.483 0 01-.923 1.785A5.969 5.969 0 006 21c1.282 0 2.47-.402 3.445-1.087.81.22 1.668.337 2.555.337z" />
          </svg>
        }
        {
          open &&
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-5 upscale-engine-h-5 md:upscale-engine-w-7 md:upscale-engine-h-7">
            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
          </svg>
        }
      </div>
    </div>
    </CSSTransition>
  )
}

export default function AppHandler() {
  const [_token,setToken] = useAtom(userToken)
  const [_user,setUser] = useAtom(user)
  const [_anonymous,setAnonymous] = useAtom(anonymous)
  const [_env] = useAtom(env)
  const storage = useStorage()
  const tokenFilled = axios.defaults.headers.common['Authorization']
  
  useEffect(() => {
    storage.init()
    let token = storage.get('token')
    let anonymous = storage.get('anonymous')
    axios.post('auth/login', {token:token,anonymous:anonymous}).then((_res) => {
      const {user,token,anonymous} = _res.data
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
      setUser(user)
      setToken(token)
      setAnonymous(anonymous)
      storage.set('token', `${token}`)
      storage.set('user', user)
      storage.set('anonymous', anonymous)
    }).catch((err) => {
      console.log(err)
    })
  }, [])

  if(!_user || !tokenFilled) {
    return null
  }

  return <App />
}
