Array.prototype.make = (args,callback) => {
  return Array.from({length: args}, (_,k) => (callback instanceof Function ? callback(k): (k+1)))
}
Array.prototype.first = function(){
  return this[this.length -1]
}