import React from 'react'
import ReactDOM from 'react-dom/client'
import AppHandler from './App'
import './services/axios'
import './index.css'
import './Array.js'
import './Object.js'

const root = document.getElementById('root')
root.classList.add('upscale-App-Root')

ReactDOM.createRoot(root).render(
  <React.StrictMode>
    <AppHandler />
  </React.StrictMode>
)
