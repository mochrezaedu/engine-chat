import { useAtom } from "jotai";
import { useState } from "react";
import { busy as derivedBusy, inputSelect as derivedInputSelect } from "../services/stores";
import LabelSelectMultiple from "./LabelSelectMultiple";
import LabelSelectMultipleVerticalList from "./LabelSelectMultipleVerticalList";

export default function InputSelect(props) {
  const {className, onChange, onSubmit, ...rest} = props
  const [value, setValue] = useState('');
  const [input, setInput] = useAtom(derivedInputSelect)
  const [busy] = useAtom(derivedBusy)

  const _handleInputChange = (args) => {
    setInput({
      ...input,
      input: {
        ...input.input,
        value: args.target.value
      }
    })
    if(onChange instanceof Function) {
      onChange(args)
    }
  }


  return (
    <>
      <div className="upscale-engine-flex-grow">
        {
          input.open &&
          (
            input.type == 'vertical_list' ? <LabelSelectMultipleVerticalList search={input.input.value} />:
            <LabelSelectMultiple search={input.input.value} />
          )
        }
        <div className="upscale-engine-w-full">
          <input disabled={input.input.status === 'disabled'} 
          placeholder={input.input.placeholder || ''} 
          className={`upscale-engine-outline-none upscale-engine-ring-0 upscale-engine-border-none upscale-engine-text-sm upscale-engine-w-full ${className || ''}`} 
          value={input.input.value || ''} 
          type={input.input.type} 
          {...rest}
          onChange={_handleInputChange} />
        </div>

      </div>
      {
        input.input.show &&
        <button onClick={() => {
          if(busy) {
            onSubmit()
          } else {
            console.log('im free')
          }
        }} className={`${(Boolean(input.input.status === 'disabled' && busy) ? 'upscale-engine-bg-gray-400': 'upscale-engine-bg-indigo-700')} upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center`} disabled={Boolean(input.input.status === 'disabled' && busy)}>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke={`${input.input.status === 'disabled' ? 'gray': 'white'}`} className="upscale-engine-w-4 upscale-engine-h-4">
            <path strokeLinecap="round" strokeLinejoin="round" d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5" />
          </svg>
        </button>
      }
    </>
  )
}