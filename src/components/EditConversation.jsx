import { useAtom } from 'jotai'
import { forwardRef, useEffect, useRef, useState } from 'react'
import DatePicker from 'react-datepicker'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { objectToCamelCase } from '../helper'
import { useAttribute } from '../services/hooks'
import {
  editConversation as derivedEditConversation,
  conversations as derivedConversations,
  initialEditConversation,
  busy
} from '../services/stores'
import axios from "axios";
import ReactLoading from 'react-loading';


const InputText = () => {
  const [editConversation,setEditConversation] = useAtom(derivedEditConversation)
  const _getInputType = () => {
    if(editConversation.item?.flow.type === 'input_text') {
      return 'text'
    } else if(editConversation.item?.flow.type === 'input_number') {
      return 'number'
    }

    return 'text'
  }
  return (
    <div>
      <input 
      type={_getInputType()}
      placeholder={'Tulis jawabanmu.'} 
      className={`upscale-engine-outline-none upscale-engine-ring-0 upscale-engine-text-sm upscale-engine-w-full upscale-engine-border upscale-engine-mb-2 upscale-engine-px-2 upscale-engine-py-1 upscale-engine-rounded-md`} 
      value={editConversation.item?.text || ''}
      onChange={(e) => {
        const item = editConversation.item
        if(item) {
          item['text'] = e.target.value
          setEditConversation({
            ...editConversation,
            item: item
          })
        }
      }} />
    </div>
  )
}
const InputDate = () => {
  const [editConversation,setEditConversation] = useAtom(derivedEditConversation)
  const [_value, setValue] = useState(new Date(editConversation.item.text))
  const ActivatorBase = forwardRef(({ value, onClick }, ref) => (
    <button className="upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-bg-indigo-600 upscale-engine-text-white upscale-engine-text-sm" onClick={onClick} ref={ref}>
      {value}
    </button>
  ));
  return (
    <DatePicker
    selected={_value}
    onChange={(date) => {
      setValue(date)
      const item = editConversation.item
      if(item) {
        item['text'] = date.toISOString()
        setEditConversation({
          ...editConversation,
          item: item
        })
      }
    }} 
    customInput={<ActivatorBase />}
    />
  )
}
const InputChoice = () => {
  const [editConversation,setEditConversation] = useAtom(derivedEditConversation)
  const [_value,setValue] = useState(editConversation.item?.flow.flow_choices.find((item) => item.text === editConversation.item?.text))
  return (
    <div>
      <div className="upscale-engine-grid upscale-engine-grid-cols-2 upscale-engine-gap-2">
        {
          editConversation.item?.flow?.flow_choices?.map((item) => {
            return (
              <button onClick={() => {
                setValue(item)
                const _oitem = editConversation.item
                if(_oitem) {
                  _oitem['text'] = item.text
                  setEditConversation({
                    ...editConversation,
                    item: _oitem
                  })
                }
              }} className={`rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-border ${_value.id === item.id ? 'upscale-engine-bg-indigo-600 upscale-engine-text-white': 'upscale-engine-border-gray-400 upscale-engine-text-gray-600'} upscale-engine-text-sm`} 
              key={item.id}>
                {item.text}
              </button>
            )
          })
        }
      </div>
    </div>
  )
}
const InputSelect = () => {
  const [editConversation,setEditConversation] = useAtom(derivedEditConversation)
  const choices = editConversation.item.flow.choices
  const [search,setSearch] = useState('')
  const [resource,setResource] = useState(editConversation.item.flow.choices)
  const [filteredResource,setFilteredResource] = useState(editConversation.item.flow.choices)
  const [selected,setSelected] = useState([])
  const [loading,setLoading] = useState(false)
  const dataObj = {}
  editConversation.item?.flow?.extras?.map((item) => {
    dataObj[item.key] = item.value
  })
  const data = objectToCamelCase(dataObj)
  const text = () => {
    if(editConversation.fetch_type === 'multiple') {
      if(editConversation.item.conversation_inputs.length > 5) {
        return (editConversation.item.conversation_inputs.map((item) => item.text).slice(0,5).join(', ')) + ', Load more' 
      }
      return editConversation.item.conversation_inputs.map((item) => item.text).join(', ')
    }
    return editConversation.item?.text
  }

  const _storeEditedItem = (args) => {
    const _cItem = editConversation.item
    if(_cItem) {
      const itemsToStore = args.map((item) => item['text']).join(', ')
      // _cItem['conversation_inputs'] = args
      _cItem['text'] = itemsToStore
      setEditConversation({
        ...editConversation,
        item: _cItem
      })
    }
  }

  const _handleClick = (args) => {
    const item = filteredResource.find((item) => item['id'] === args['id'])
    const alreadyInserted = selected.filter((item) => item['id'] === args['id'])
    if(item && alreadyInserted.length < 1) {
      const newItems = [...selected, item]
      setSelected(newItems)
      _storeEditedItem(newItems)
    } else {
      const newItems = selected.filter((item) => item['id'] !== args['id'])
      setSelected([...newItems])
      _storeEditedItem(newItems)
    }

  }

  const _isSelected = (args) => {
    const item = selected.findIndex((item) => item['id'] === args['id'])
    return item > -1
  }

  useEffect(() => {
    const selectedFromInput = editConversation.item?.conversation_inputs?.map((item) => item.text)
    if(selectedFromInput) {
      const fillSelected = choices.filter((item) => selectedFromInput.includes(item['text']))
      setSelected(fillSelected)
    }
    setLoading(false)
  }, [])

  useEffect(() => {
    if(search != '' && search.length > 0) {
      const filteredItems = resource.filter((item) => {
        const _fLabel = item['text']?.toLowerCase().indexOf(search) > -1
        const _fId = item['id']?.toString().toLowerCase().indexOf(search) > -1
        return _fLabel || _fId
      })
      setFilteredResource([...filteredItems])
    } else {
      setFilteredResource([...resource])
    }
  }, [search])

  return (
    <div>
      <input 
      placeholder={'Search'} 
      className={`outline-none upscale-engine-ring-0 upscale-engine-text-sm upscale-engine-w-full upscale-engine-border upscale-engine-mb-2 upscale-engine-px-2 upscale-engine-py-1 upscale-engine-rounded-md`} 
      value={search}
      onChange={(e) => setSearch(e.target.value)} 
      type={'text'} />
      {
        loading && 
        <div className="upscale-engine-flex upscale-engine-items-center upscale-engine-justify-center">
          <ReactLoading type={'spin'} color={'rgb(55 65 81)'} height={'1.75rem'} width={'1.75rem'} />
        </div>
      }
      {
        !loading && selected.length > 0 &&
        <div className='upscale-engine-flex upscale-engine-items-center upscale-engine-justify-between upscale-engine-mb-2'>
          <button onClick={() => {
            _storeEditedItem([])
            setSelected([])
          }} className='upscale-engine-text-xs upscale-engine-rounded-full upscale-engine-bg-red-700 upscale-engine-text-white upscale-engine-px-3 upscale-engine-py-1'>Clear Selection</button>
          <div className='upscale-engine-text-xs upscale-engine-text-gray-500'>{selected.length} Selected</div>
        </div>
      }
      <div className='upscale-engine-overflow-y-auto upscale-engine-pt-2 md:upscale-engine-max-h-[300px]'>
        {
          !loading && 
          filteredResource.map((item) => <button key={item['id']} 
          onClick={() => _handleClick(item)}
          className={`${_isSelected(item) ? 'upscale-engine-bg-indigo-700 upscale-engine-text-white': 'upscale-engine-border upscale-engine-border-gray-400'} upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-mr-1 upscale-engine-mb-1 upscale-engine-inline-block upscale-engine-text-gray-700`}>
            {item['text']}</button>)
        }
      </div>
    </div>
  )
}

export default function EditConversation(props) {
  const {className} = props
  const [editConversation,setEditConversation] = useAtom(derivedEditConversation)
  const [conversations,setConversations] = useAtom(derivedConversations)
  const [confirmed,setConfirmed] = useState(false)
  const [confirmation,setConfirmation] = useState(false)
  const [loading,setLoading] = useState(false)
  const panelRef = useRef(null)
  const [,setBusy] = useAtom(busy)

  const _tr_ = useRef(null)
  const {attributeReplacer} = useAttribute()
  
  const _canSubmit = () => {
    return editConversation.item && (editConversation.item?.text?.length > 0 && editConversation.item?.text != '')
  }
  const _handleSubmit = async () => {
    const oldConversations = conversations
    const itemIndex = oldConversations.findIndex((item) => item.id === editConversation.item.id)
    if(itemIndex > -1) {
      oldConversations[itemIndex] = editConversation.item
      const updating = await axios.post('conversation/update', editConversation.item)
      setConversations([...oldConversations])
      const editedData = updating.data
      if(editedData) {
        const currentConversationIndex = conversations.findIndex((item) => item.id === editedData.id)
        if(currentConversationIndex > -1) {
          const newSlicedConversations = [...conversations.slice(0,currentConversationIndex+1)]
          setConversations(newSlicedConversations)
        }
      }
    }
    setConfirmation(false)
    setConfirmed(false)
    setEditConversation(initialEditConversation)
    setBusy(true)
    setLoading(false)
  }

  useEffect(() => {
    if(confirmed) {
      setLoading(true);
      _handleSubmit()
    }
  }, [confirmed])

  useEffect(() => {
    return () => {
      setLoading(false)
    }
  }, [])

  return (
    <CSSTransition onEnter={() => console.log('fired')} nodeRef={_tr_} 
    in={editConversation.show} timeout={200} classNames={'modal'}>
      <div ref={_tr_} className={`upscale-engine-relative upscale-engine-z-50 upscale-engine-bg-white upscale-engine-rounded-md upscale-engine-flex upscale-engine-flex-col upscale-engine-shadow-md -upscale-engine-translate-y-full upscale-engine-top-0 ${className} upscale-engine-w-full upscale-engine-h-screen md:upscale-engine-w-[330px] md:upscale-engine-h-[460px]`}>
        <div className='upscale-engine-flex upscale-engine-justify-between upscale-engine-items-center upscale-engine-p-3 upscale-engine-pb-2 upscale-engine-border-b'>
          <div className='upscale-engine-break-all upscale-engine-pr-2'>Update <span className='upscale-engine-font-medium'>Answer</span></div>
          <div className='upscale-engine-flex upscale-engine-items-center'>
            {
              _canSubmit() &&
              <button onClick={() => setConfirmation(true)}
              className={`upscale-engine-mr-1 upscale-engine-text-sm upscale-engine-bg-indigo-700 upscale-engine-text-white upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center`} 
              disabled={!_canSubmit()}>
                Save
              </button>
            }
            <button onClick={() => {
              setConfirmation(false)
              setConfirmed(false)
              setEditConversation(initialEditConversation)
            }} className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center'>
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4">
                <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 19.5l15-15m-15 0l15 15" />
              </svg>
            </button>
          </div>
        </div>
        <div className='upscale-engine-flex-grow upscale-engine-p-3'>
          {
            confirmation &&
            <div className="upscale-engine-bg-orange-100 upscale-engine-border-l-4 upscale-engine-border-orange-500 upscale-engine-text-orange-700 upscale-engine-p-4 upscale-engine-mb-2" role="alert">
              <p className="upscale-engine-font-bold upscale-engine-text-md">Perhatian</p>
              <p className='upscale-engine-text-sm'>Jika mengedit jawaban, chat setelahnya akan dihapus.</p>
              <div className='upscale-engine-mt-2 upscale-engine-flex upscale-engine-items-center upscale-engine-space-x-1'>
                <button onClick={() => setConfirmed(true)}
                className={`mr-1 upscale-engine-text-sm upscale-engine-bg-red-700 upscale-engine-text-white upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center`} 
                disabled={!_canSubmit()}>
                  {
                    !loading ? 'Lanjutkan':
                    <ReactLoading type={'spin'} color={'white'} height={'1.25rem'} width={'1.25rem'} />
                  }
                </button>
                <button onClick={() => {
                  setConfirmation(false)
                  setConfirmed(false)
                }}
                className={`mr-1 upscale-engine-text-sm upscale-engine-bg-gray-700 upscale-engine-text-white upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center`} 
                disabled={!_canSubmit()}>
                  Batal
                </button>
              </div>
            </div>
          }
          <div className='upscale-engine-font-medium upscale-engine-mb-3 upscale-engine-flex upscale-engine-items-center upscale-engine-justify-between'>
            <div>{attributeReplacer(editConversation.item?.flow?.response)}</div>
          </div>
          {
            ['input_text','input_number'].includes(editConversation.item?.flow.type) &&
            <InputText />
          }
          {
            editConversation.item?.flow.type === 'input_date' &&
            <InputDate />
          }
          {
            editConversation.item?.flow.type === 'input_choice' &&
            <InputChoice />
          }
          {
            editConversation.item?.flow.type === 'input_multi_search' &&
            <InputSelect />
          }
        </div>
      </div>
    </CSSTransition>
  )
}