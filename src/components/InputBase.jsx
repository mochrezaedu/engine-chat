import { useAtom } from "jotai"
import { busy as derivedBusy, inputBase, conversations as derivedConversations } from "../services/stores"
import LabelBase from "./LabelBase"

export default function InputBase(props) {
  const {className, withLabel, onSubmit, value, mode, disableButton, ...rest} = props
  const [input] = useAtom(inputBase)
  const [busy] = useAtom(derivedBusy)

  const _handleSubmit = (args) => {
    onSubmit()
  }

  return (
    <>
      <div className="upscale-engine-flex-grow upscale-engine-flex upscale-engine-items-center">
        {
          input.label && 
          <LabelBase />
        }
        <input placeholder={input.placeholder || (!busy ? 'Tulis Chat': '')} className={`upscale-engine-outline-none upscale-engine-ring-0 upscale-engine-border-none upscale-engine-text-sm upscale-engine-w-full upscale-engine-bg-transparent ${className||''}`} {...rest} value={(mode=='manual' ? value: input.value || '')} type={(mode=='manual' ? 'text':input.type)} />
      </div>
      {
        input.show && !disableButton && 
        <button onClick={_handleSubmit} className={`${Boolean(input.status == 'disabled' && busy) ? 'upscale-engine-bg-gray-200': 'upscale-engine-bg-indigo-700'} upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center`} disabled={Boolean(input.status === 'disabled' && busy)}>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke={`${Boolean(input.status === 'disabled' && busy) ? 'gray': 'white'}`} className="upscale-engine-w-4 upscale-engine-h-4">
            <path strokeLinecap="round" strokeLinejoin="round" d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5" />
          </svg>
        </button>
      }
    </>
  )
}