import { useAtom } from "jotai";
import ChatItemLeft from "./ChatItemLeft"
import ChatItemRight from "./ChatItemRight"
import { 
  appendConversations, 
  conversations as derivedConversations, 
  flows as derivedFlows,
  isTyping as derivedIsTyping,
  inputBase as input,
  inputChoice as derivedInputChoice,
  inputSelect as derivedInputMultiSelect,
  inputDate as derivedInputdate,
  user,
  editConversation as derivedEditConversation,
  busy as derivedBusy
} from "../services/stores";
import ReactLoading from "react-loading";
import { useEffect, useMemo, useState } from "react";
import axios from "axios";
import { objectToCamelCase, timeSince } from "../helper";
import InputChoice from "./InputChoice";
import { useCsSession, useInputManager, useNextManager, useNlpSample, useRecommendation, useTalking } from "../services/hooks";
import InputDateActivator from "./InputDate";
import EditConversation from "./EditConversation";
import WikipediaDropdown from "./WikipediaDropdown";
import ChatItemText from "./ChatItemText";
import TextImage from "./TextImage";
import TextImageEditor from "./TextImageEditor";
import FreeChatRecomendation from "./FreeChatRecomendation";
import ChatConnectCostumerService from "./ChatConnectCostumerService";

const _isSender = (forwardItem) => forwardItem.type === 'sender'
const _isReceiver = (forwardItem) => forwardItem.type === 'receiver'

const RenderChat = (props) => {
  const {forwardItem} = props
  return (
    <>
      {
        _isReceiver(forwardItem) &&
        <>
          {
            Boolean(forwardItem.flow.type === 'wiki_dropdown') &&
            <ChatItemLeft withAnimation={true} forwardItem={forwardItem} boxClassName={'upscale-engine-flex-grow'}><WikipediaDropdown forwardItem={forwardItem} /></ChatItemLeft>
          }
          {
            Boolean(forwardItem.flow.type === 'text_image') &&
            <ChatItemLeft withAnimation={true} forwardItem={forwardItem} boxClassName={'upscale-engine-flex-grow-0'}><TextImage forwardItem={forwardItem} /></ChatItemLeft>
          }
          {
            Boolean(forwardItem.flow.type === 'text_image_editor_list') &&
            <ChatItemLeft withAnimation={true} forwardItem={forwardItem} boxClassName={'upscale-engine-flex-grow-0'}><TextImageEditor forwardItem={forwardItem} /></ChatItemLeft>
          }
          {
            !['wiki_dropdown', 'text_image', 'text_image_editor_list'].includes(forwardItem.flow.type) &&
            <ChatItemLeft withAnimation={true} forwardItem={forwardItem} />
          }
        </>
      }
      {
        _isSender(forwardItem) &&
        <ChatItemRight withAnimation={true} forwardItem={forwardItem} />
      }
    </>
  )
}

export default function ChatList() {
  const [,setUser] = useAtom(user)
  const [,setInput] = useAtom(input)
  const [inputChoice,setInputChoice] = useAtom(derivedInputChoice)
  const [inputMultiSelect,setInputMultiSelect] = useAtom(derivedInputMultiSelect)
  const [inputDate,setInputDate] = useAtom(derivedInputdate)
  const {resetInputs} = useInputManager()
  const [flows,setFlows] = useAtom(derivedFlows)
  const [isTyping,setIsTyping] = useAtom(derivedIsTyping)
  const [busy] = useAtom(derivedBusy)
  const recommendation = useRecommendation()

  const [conversations, setConversations] = useAtom(derivedConversations)
  const lastConversationId = useMemo(() => conversations[conversations.length-1]?.id, [conversations])
  const lastConversation = useMemo(() => conversations[conversations.length-1], [conversations])
  const _automaticNext = useNextManager().automaticNext
  const _talk = useTalking().send
  const nlpSample = useNlpSample()

  const _getFlowItem = () => {
    return (lastConversation ? lastConversation.flow: flows[0])
  }
  const _getNextFlowItem = () => {
    return flows.find((item) => (item.id == _getFlowItem().next || item.name == _getFlowItem().next))
  }
  const _handleChangeChoice = (item) => {
    resetInputs()
    axios.post('/conversation/store', {
      "flow": {id: lastConversation.flow.id}, "next": item.next, "type": "sender", "text": item.text
    }).then((_res) => {
      appendConversations({
        payload: _res.data,
        state: conversations,
        setState: setConversations
      })
    }).catch((_err) => {
      alert('Error while store Conversation')
    })
  }

  useEffect(() => {
    if(typeof lastConversationId !== 'undefined') {
      if(_isReceiver(lastConversation)) {
        setIsTyping(false)
        switch (lastConversation.flow.type) {
          case 'wiki_dropdown':
            resetInputs()
            _automaticNext()
          case 'text':
            resetInputs()
            _automaticNext()
            break;
          case 'input_multi_choice_vertical_list':
            resetInputs()
            const _o = {}
            lastConversation.flow?.extras?.map((item) => {
              _o[item.key] = item.value
            })
            axios.get(`/flow/${lastConversation.flow.id}/choices`).then((_res) => {
              const data = objectToCamelCase(_o)
              setInput({...input,show: false})
              setInputMultiSelect({
                ...inputMultiSelect,
                ...data,
                isLoadingInputResources: true,
                resources: [..._res.data],
                filteredResources: [..._res.data],
                input: {
                  ...inputMultiSelect.input,
                  status: 'enabled'
                },
                show: true,
                type: 'vertical_list'
              })
            }).catch((_err) => {
              alert('Error while get choices.')
              console.log(_err)
            })
            
            break;
          case 'input_multi_search':
            resetInputs()
            const dataObj = {}
            lastConversation.flow?.extras.map((item) => {
              dataObj[item.key] = item.value
            })
            axios.get(`/flow/${lastConversation.flow.id}/choices`).then((_res) => {
              const data = objectToCamelCase(dataObj)
              setInput({...input,show: false})
              setInputMultiSelect({
                ...inputMultiSelect,
                ...data,
                isLoadingInputResources: true,
                resources: [..._res.data],
                filteredResources: [..._res.data],
                input: {
                  ...inputMultiSelect.input,
                  status: 'enabled'
                },
                show: true,
                type: 'basic'
              })
            }).catch((_err) => {
              alert('Error while get choices.')
              console.log(_err)
            })
            
            break;
          case 'input_text':
            resetInputs()
            const t__input_data = lastConversation.flow?.extras.find((item) => item.key === 'input_label')
            setInput({
              placeholder: 'Tulis jawaban anda.',
              type: 'text',
              value: '',
              label: t__input_data?.value,
              status: 'enabled',
              show: true
            })
            break;
          case 'input_date':
            resetInputs()
            const d__input_data = lastConversation.flow?.extras.find((item) => item.key === 'input_label')
            setInputDate({
              ...inputDate,
              type: 'date',
              value: new Date(),
              status: 'enabled',
              label: d__input_data?.value,
              show: true
            })
            break;
          case 'input_number':
            resetInputs()
            const n__input_data = lastConversation.flow?.extras.find((item) => item.key === 'input_label')
            setInput({
              ...input,
              type: 'number',
              value: null,
              status: 'enabled',
              placeholder: 'Input Number',
              label: n__input_data?.value,
              show: true
            })
            break;
          case 'input_choice':
            resetInputs()
            axios.get(`/flow/${lastConversation.flow.id}/choices`).then((_res) => {
              setInputChoice({
                options: [..._res.data],
                label: lastConversation.flow?.data?.input_label,
                show: true
              })
            }).catch((_err) => {
              alert('Error while get choices.')
              console.log(_err)
            })
            break;
          default:
            _automaticNext()
            // setIsTyping(false)
            break;
        }
      }
      if(_isSender(lastConversation)) {
        if(busy) {
          resetInputs()
          _automaticNext()
        } else {
          recommendation.resetChatRecommendation()
          _talk().then((_r) => {
            // if(_r.data.length > 0) {
              
            // }
            nlpSample.setInput(lastConversation.text)
            recommendation.setChatRecommendations(_r.data)
          }).catch(err => {
            console.log(err)
          })
        }
      }
    }
  }, [lastConversationId])


  return (
    <div className="upscale-engine-flex upscale-engine-flex-col upscale-engine-space-y-2">
      {
        conversations.map((item) => {
          item.time_converted = timeSince(item.created_at)
          return (
            item.text == '' ? null: <RenderChat forwardItem={item} key={item.id} />
          )
        })
      }
      {
        inputMultiSelect.show &&
        <ChatItemRight editable={false} forwardItem={lastConversation}>
          <button onClick={() => {
            setInputMultiSelect({
              ...inputMultiSelect,
              isLoadingInputResources: true,
              open: true
            })
          }} className="upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-bg-indigo-600 upscale-engine-text-white upscale-engine-text-sm">
            Open Input
          </button>
        </ChatItemRight>
      }
      {
        inputChoice.show &&
        <ChatItemRight editable={false} forwardItem={lastConversation} noBackground={'sdf'}>
          <InputChoice onChange={(item) => {
            _handleChangeChoice(item)
          }} />
        </ChatItemRight>
      }
      {
        inputDate.show &&
        <ChatItemRight editable={false} forwardItem={lastConversation}>
          <InputDateActivator onSubmit={() => {
            const storeInputDate = inputDate
            resetInputs()
            axios.post('/conversation/store', {
              "flow": {id: lastConversation.flow.id}, "next": lastConversation.flow.next, "type": "sender", "text": inputDate.value
            }).then((_res) => {
              appendConversations({
                payload: _res.data,
                state: conversations,
                setState: setConversations
              })
            }).catch((_err) => {
              alert('Error while store Conversation')
              setInputDate({...storeInputDate})
            })
          }} />
        </ChatItemRight>
      }
      {
        recommendation.show && recommendation.data.length > 0 &&
        <ChatItemLeft>
          <FreeChatRecomendation />
        </ChatItemLeft>
      }
      {
        recommendation.show && recommendation.data.length == 0 &&
        <ChatItemLeft>
          <ChatConnectCostumerService />
        </ChatItemLeft>
      }

      {
        isTyping &&
        <ChatItemLeft forwardItem={lastConversation}>
          <div className="upscale-engine-border upscale-engine-border-gray-200 upscale-engine-rounded-full">
            <ChatItemText text={
              <div className="upscale-engine-h-[40px] ">
                <ReactLoading type={'bubbles'} color={'#858585'} height={'0px'} width={'40px'} />
              </div>
            } 
            />
          </div>
        </ChatItemLeft>
      }
    </div>
  )
}

var chats = Array.from({length: 5}, (_,i) => ({id: Math.floor(Math.random() * 99999999), response: `chat ke ${Math.floor(Math.random() * 99999999)}`}))
var chats_right = Array.from({length: 5}, (_,i) => ({id: Math.floor(Math.random() * 99999999), response: `chat ke ${Math.floor(Math.random() * 99999999)}`}))
