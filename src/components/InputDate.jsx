import { useAtom } from "jotai";
import { forwardRef, useState } from "react";
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css'
import { inputDate } from "../services/stores";

export default function InputDateActivator(props) {
  const {onEdit,onSubmit} = props
  const [input,setInput] = useAtom(inputDate)
  const [filled,setFilled] = useState(false)
  const ActivatorBase = forwardRef(({ value, onClick }, ref) => (
    <button className="upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-bg-indigo-700 upscale-engine-text-white upscale-engine-text-sm" onClick={onClick} ref={ref}>
      {input.value != '' ? input?.label: value}
    </button>
  ));
  return (
    <div className="upscale-engine-flex upscale-engine-items-center upscale-engine-space-x-1">
      <DatePicker
      peekNextMonth
      showMonthDropdown
      showYearDropdown
      dropdownMode="select"
      selected={input.value}
      onChange={(date) => {
        setInput({...input, value: date})
        onSubmit()
      }} 
      customInput={<ActivatorBase />}
      />
    </div>
  )
}