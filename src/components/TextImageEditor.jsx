import { Fragment, useEffect } from "react"
import { timeSince } from "../helper"

function RenderImages (props) {
  const {forwardItem} = props
  const images = forwardItem.images
  return (
    <div className={`${images.length > 0 ? `upscale-engine-flex upscale-engine-space-x-2 upscale-engine-min-w-[${200*images.length}px]`:''}`}>
      {
        images.length === 1 &&
        <a href={`${import.meta.env.VITE_API_URL}/${images[0]?.path}`} target={'_blank'} className="upscale-engine-w-[200px] upscale-engine-mb-2">
          <img className="upscale-engine-w-full upscale-engine-max-w-1/2 upscale-engine-rounded-md" src={`${import.meta.env.VITE_API_URL}/${images[0]?.path}`} alt="" />
        </a>
      }
      {
        images.length > 1 &&
        images.map((item) => (
          <a href={`${import.meta.env.VITE_API_URL}/${item?.path}`} target={'_blank'} className="upscale-engine-mb-2" key={item.id}>
            <img className="upscale-engine-w-[200px] upscale-engine-max-w-[200px] upscale-engine-h-[134px] upscale-engine-rounded-md" src={`${import.meta.env.VITE_API_URL}/${item.path}`} alt="" />
          </a>
        ))
      }
    </div>
  )
}

export default function TextImageEditor(props) {
  const {forwardItem} = props
  const list = forwardItem.flow.text_image_editor_list
  
  useEffect(() => {
    console.log(list)
  }, [])

  return (
    <div className="upscale-engine-justify-start upscale-engine-max-w-full">
      <div className="upscale-engine-rounded-md upscale-engine-border upscale-engine-py-2 upscale-engine-px-2" style={{flex:1}}>
        <div className="upscale-engine-flex upscale-engine-flex-col upscale-engine-space-y-2">
        {
          list.map((item) => {
            return (
              <Fragment key={item.id}>
                {
                  item.type === 'text' &&
                  <div className="upscale-engine-mb-1 upscale-engine-text-sm upscale-engine-font-medium upscale-engine-flex-grow-0 upscale-engine-flex">{item.content}</div>
                }
                {
                  item.type === 'image' &&
                  <RenderImages forwardItem={item} />
                }
              </Fragment>
            )
          })  
        }
        </div>
        <div className="upscale-engine-flex upscale-engine-justify-end upscale-engine-items-center">
          <div className="upscale-engine-text-gray-400" style={{fontSize: '0.57rem'}}>{timeSince(forwardItem.created_at)}</div>
        </div>
      </div>
    </div>
  )
} 