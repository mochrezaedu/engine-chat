import { useAtom } from "jotai"
import { useEffect } from "react"
import { useNextManager } from "../services/hooks"
import { conversations as derivedConversations, editConversation as derivedEditConversation } from "../services/stores"
import ChatItemText from "./ChatItemText"

export default function ChatItemRight(props) {
  const {forwardItem,children,editable,noBackground,withAnimation} = props
  const [editConversation,setEditConversation] = useAtom(derivedEditConversation)
  
  const text = () => {
    return forwardItem.text
  }
  const time = forwardItem.created_at

  const _handleEditConversation = () => {
    setEditConversation({
      show: true,
      item: {...forwardItem}
    })
  }
  return (
    <div className={`upscale-engine-flex upscale-engine-justify-end ${withAnimation && 'chat-item-animation-container'}`}>
      {
        (editable !== false || editConversation.show) && forwardItem.is_free != 'yes' &&
        <button className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-5 upscale-engine-h-5 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center upscale-engine-mr-1' 
        onClick={_handleEditConversation}>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-3 upscale-engine-h-3">
            <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
          </svg>
        </button>
      }
      <div className={`upscale-engine-rounded-md upscale-engine-p-1 ${(editable !== false || editConversation.show) ? 'upscale-engine-border upscale-engine-bg-gray-200':''}`} style={{maxWidth: '92%'}}>
        {
          children || <ChatItemText title={'You'} text={text()} time={time} />
        }
      </div>
    </div>
  )
}