import { useAtom } from "jotai"
import { inputChoice as derivedInputChoice } from "../services/stores"
export default function InputChoice(props) {
  const {onChange} = props
  const [inputChoice] = useAtom(derivedInputChoice)

  return (
    <div className="upscale-engine-px-2">
      <div className="upscale-engine-mb-2 upscale-engine-text-sm upscale-engine-font-medium">{inputChoice.label}</div>
      <div className="upscale-engine-flex upscale-engine-flex-col upscale-engine-space-y-2">
        {
          inputChoice.options.map((item,index) => {
            return (
              <button onClick={() => onChange(item)} className="upscale-engine-rounded upscale-engine-px-3 upscale-engine-py-1 bg-white-700 upscale-engine-text-indigo-700 upscale-engine-text-sm upscale-engine-border upscale-engine-border-indigo-700 hover:upscale-engine-bg-indigo-700 hover:upscale-engine-text-white" 
              key={item.id}>
                {item.text}
              </button>
            )
          })
        }
      </div>
    </div>
  )
}