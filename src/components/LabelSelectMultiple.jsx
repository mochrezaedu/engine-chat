import { useEffect, useState } from "react";
import LabelBase from "./LabelBase";
import ReactLoading from 'react-loading';
import { inputSelect as derivedInputMultiSelect } from "../services/stores";
import { useAtom } from "jotai";
import axios from "axios";

export default function LabelSelectMultiple(props) {
  const {search} = props
  const [windowPosition, setWindowPosition] = useState('maximized')
  const [inputSelect,setInputSelect] = useAtom(derivedInputMultiSelect)
  
  const _handleMinimize = () => {
    setWindowPosition('minimized')
  }
  const _handleMaximize = () => {
    setWindowPosition('maximized')
  }
  const _handleChange = (args) => {
    if(args.id) {
      const find = inputSelect.selected.find((item) => item.id === args.id)
      if(!find) {
        var oSelected = inputSelect.selected;
        const popFilteredItems = inputSelect.filteredResources.filter((item) => item.id !== args.id)
        setInputSelect({
          ...inputSelect, selected: [...oSelected, args], filteredResources: [...popFilteredItems]
        })
      }
      
    }
  }
  const _handleDelete = (args) => {
    if(args.id) {
      const find = inputSelect.selected.find((item) => item.id === args.id)
      if(find) {
        var oSelected = inputSelect.selected.filter((item) => item.id !== args.id);
        setInputSelect({
          ...inputSelect, selected: [...oSelected], filteredResources: [...inputSelect.filteredResources, args]
        })
      }
    }
  }
  const _handleSubmit = () => {
    
  }

  const isMinimized = () => {
    return windowPosition === 'minimized';
  }
  const isMaximized = () => {
    return windowPosition === 'maximized';
  }
  useEffect(() => {
    const newFilterItems = {...inputSelect}
    if(inputSelect.input.value != '' && inputSelect.input.value?.length > 0) {
      const sItems = inputSelect.resources.filter((item) => {
        return item.text?.toLowerCase().indexOf(inputSelect.input.value?.toLowerCase()) > -1;
      })
      newFilterItems['filteredResources'] = sItems
    } else {
      newFilterItems['filteredResources'] = inputSelect.resources
    }

    setInputSelect({
      ...newFilterItems,
      isLoadingInputResources: false,
      input: {
        ...inputSelect.input,
      }
    })
  }, [inputSelect.input.value])

  useEffect(() => {

  }, [])

  return (
    <LabelBase block={true} className={`upscale-engine-flex upscale-engine-flex-col ${isMinimized() ? 'upscale-engine-h-auto':'upscale-engine-h-[86.5vh] md:upscale-engine-h-[391px]' }`}>
      <div className={`upscale-engine-border-b upscale-engine-border-white upscale-engine-pb-1 ${isMaximized() && 'upscale-engine-mb-2'}`}>
        <div className="upscale-engine-flex upscale-engine-justify-between upscale-engine-items-center upscale-engine-px-2 upscale-engine-py-1">
          <div style={{fontFamily: 'roboto'}} className={'upscale-engine-font-bold upscale-engine-text-sm'}>{inputSelect['inputLabel']}</div>
          {
            isMaximized() &&
            <button className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center' onClick={_handleMinimize}>
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4">
                <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
              </svg>
            </button>
          }
          {
            isMinimized() &&
            <button className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center upscale-engine-ml-1' onClick={_handleMaximize}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" className="upscale-engine-w-4 upscale-engine-h-4">
                <path fillRule="evenodd" d="M14.77 12.79a.75.75 0 01-1.06-.02L10 8.832 6.29 12.77a.75.75 0 11-1.08-1.04l4.25-4.5a.75.75 0 011.08 0l4.25 4.5a.75.75 0 01-.02 1.06z" clipRule="evenodd" />
              </svg>
            </button> 
          }
        </div>
      </div>
      <div className={`upscale-engine-text-sm upscale-engine-px-3 upscale-engine-py-1 upscale-engine-mr-2 upscale-engine-mb-2 upscale-engine-flex-grow upscale-engine-overflow-y-auto ${isMinimized() ? 'upscale-engine-hidden': ''}`} style={{paddingBottom: 45}}>
        {
          inputSelect.isLoadingInputResources &&
          <div className="upscale-engine-flex upscale-engine-items-center upscale-engine-justify-center">
            <ReactLoading type={'spin'} color={'rgb(55 65 81)'} height={'1.75rem'} width={'1.75rem'} />
          </div>
        }
        {
          inputSelect.isLoadingInputResources === false &&
          inputSelect.filteredResources.map((item) => {
            return (
              <button onClick={() => _handleChange(item)} key={item.id} className="upscale-engine-bg-gray-300 upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-mr-1 upscale-engine-mb-1 upscale-engine-inline-block upscale-engine-text-black hover:upscale-engine-bg-indigo-700 hover:upscale-engine-text-white hover:upscale-engine-ring-2 hover:upscale-engine-ring-indigo-700">
                {item.text}
              </button>
            )
          })
        }
      </div>
      {
        inputSelect.selected.length > 0 &&
        <div className="upscale-engine-relative">
          <div className="upscale-engine-flex upscale-engine-items-center upscale-engine-space-x-2 upscale-engine-border-t upscale-engine-border-white upscale-engine-py-2 upscale-engine-overflow-x-auto">
            <div className="upscale-engine-flex upscale-engine-space-x-1">
              {
                inputSelect.selected.map((item) => (
                  <button onClick={() => _handleDelete(item)} key={item.id} className="upscale-engine-flex upscale-engine-items-center upscale-engine-text-sm upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-bg-indigo-700 upscale-engine-text-white upscale-engine-whitespace-nowrap">
                    <span>{item.text}</span>
                    <span className="upscale-engine-ml-1">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-3 upscale-engine-h-3">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 19.5l15-15m-15 0l15 15" />
                      </svg>
                    </span>
                  </button>
                ))
              }
            </div>
            {/* <div className="upscale-engine-absolute upscale-engine-right-0 upscale-engine-top-0 upscale-engine-pr-2 upscale-engine-mt-2 upscale-engine-flex upscale-engine-items-center">
              <button className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-7 upscale-engine-h-7 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center'>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                </svg>
              </button>
            </div> */}
          </div>
        </div>
      }
    </LabelBase>
  )
}