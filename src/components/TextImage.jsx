import { timeSince } from "../helper"

export default function TextImage(props) {
  const {forwardItem} = props
  const image = forwardItem.flow.images.first()
  return (
    <div className="upscale-engine-justify-start">
      <div className="upscale-engine-rounded-md upscale-engine-border upscale-engine-py-2 upscale-engine-px-2">
        <div className="upscale-engine-mb-1 upscale-engine-text-sm upscale-engine-font-medium upscale-engine-flex-grow-0 upscale-engine-flex">{forwardItem.text}</div>
        <div className="upscale-engine-w-[200px] upscale-engine-mb-2">
          <img className="upscale-engine-w-full max-w-1/2 upscale-engine-rounded-md" src={`${import.meta.env.VITE_API_URL}/${image.path}`} alt="" />
        </div>
        <div className="upscale-engine-flex upscale-engine-justify-end upscale-engine-items-center">
          <div className="upscale-engine-text-gray-400" style={{fontSize: '0.57rem'}}>{timeSince(forwardItem.created_at)}</div>
        </div>
      </div>
    </div>
  )
} 