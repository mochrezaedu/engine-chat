import { useAtom } from "jotai"
import { inputBase } from "../services/stores"

export default function LabelBase(props) {
  const {children,block,className, ...rest} = props
  const [input] = useAtom(inputBase)
  return (
    <div {...rest} className={`upscale-engine-z-50 upscale-engine-text-sm upscale-engine-font-medium upscale-engine-absolute upscale-engine-top-0 -upscale-engine-translate-y-full upscale-engine-bg-gray-100 upscale-engine-w-full upscale-engine-left-0 upscale-engine-px-3 upscale-engine-py-1 ${Boolean(block) ? '': 'upscale-engine-flex upscale-engine-items-center upscale-engine-justify-between'} ${className || ''}`}>
      {children || input.label || ''}
    </div>
  )
}