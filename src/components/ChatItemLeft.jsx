import { useAttribute } from "../services/hooks"
import ChatItemText from "./ChatItemText"

export default function ChatItemLeft(props) {
  const {forwardItem,children,className,boxClassName,withAnimation} = props
  const {attributeReplacer} = useAttribute()
  const text = attributeReplacer(forwardItem?.text)
  const time = forwardItem?.created_at
  return (
    <div className={`upscale-engine-flex upscale-engine-justify-start upscale-engine-w-full upscale-engine-overflow-x-auto ${className || ''} ${withAnimation && 'chat-item-animation-container'}`}>
      <div className={`upscale-engine-text-left upscale-engine-rounded-md ${children ? '': 'upscale-engine-p-1 upscale-engine-border upscale-engine-border-gray-300 upscale-engine-border-solid'} ${boxClassName || ''}`}>
        {
          children || <ChatItemText title={'You'} text={text} time={time} />
        }
      </div>
    </div>
  )
}