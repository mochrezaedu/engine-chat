import {timeSince} from '../helper'
import { useCsSession, useRecommendation } from '../services/hooks'
export default function ChatConnectCostumerService() {
  const recommendation = useRecommendation()
  const csSession = useCsSession()

  const _handleClose = () => {
    recommendation.resetChatRecommendation()
  }
  const _handleSubmit = () => {
    csSession.set({}, 'connecting')
    recommendation.resetChatRecommendation()
    setTimeout(() => {
      csSession.set({}, 'connected')
      setTimeout(() => {
        csSession.resetCsSession()
      }, 5000)
    }, 1000)
  }

  return (
    <div className='upscale-engine-text-left upscale-engine-rounded-md upscale-engine-p-1 upscale-engine-border upscale-engine-border-gray-300 upscale-engine-border-solid'>
      <div className="upscale-engine-text-sm upscale-engine-px-2">
        <div>Maaf, permintaan anda tidak dikenali</div>
        <div>Ingin chat dengan CS ?</div>
        <div className='upscale-engine-justify-start upscale-engine-flex upscale-engine-items-center upscale-engine-w-full upscale-engine-mt-3'>
          <button 
          onClick={() => _handleSubmit()}
          className={`upscale-engine-text-sm upscale-engine-bg-indigo-700 upscale-engine-text-white upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center`}
          >
            Ya
          </button>
          <button 
          onClick={() => _handleClose()}
          className={`upscale-engine-text-sm upscale-engine-text-red-500 upscale-engine-rounded-full upscale-engine-px-3 upscale-engine-py-1 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center`}
          >
            Tidak
          </button>
        </div>
      </div>
      {/* <div className="upscale-engine-flex upscale-engine-justify-end upscale-engine-items-center">
        <div className="upscale-engine-text-gray-400" style={{fontSize: '0.57rem'}}>{timeSince((new Date()).toString())}</div>
      </div> */}
    </div>
  )
}