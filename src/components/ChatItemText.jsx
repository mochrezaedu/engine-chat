import { useEffect, useState } from 'react'
import {timeSince} from '../helper'
export default function ChatItemText(props) {
  const {title,text,time} = props

  return (
    <>
      <div className="upscale-engine-text-sm upscale-engine-px-2">
        {text}
      </div>
      {
        Boolean(time) &&
        <div className="upscale-engine-flex upscale-engine-justify-end upscale-engine-items-center">
          <div className="upscale-engine-text-gray-400" style={{fontSize: '0.57rem'}}>{timeSince(time)}</div>
        </div>
      }
    </>
  )
}