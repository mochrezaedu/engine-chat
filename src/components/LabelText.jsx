import LabelBase from './LabelBase'

export default function LabelText() {
  return (
    <LabelBase>
      <div className='upscale-engine-text-sm'>Input adalah</div>
      <button className='upscale-engine-bg-white upscale-engine-rounded-full upscale-engine-px-2 upscale-engine-text-xs'>
        Sembunyikan
      </button>
    </LabelBase>
  )
}