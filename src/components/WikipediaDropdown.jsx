import { useState } from "react"

export default function WikipediaDropdown(props) {
  const {forwardItem} = props
  const [activation, setActivation] = useState([])
  return (
    <div className="upscale-engine-justify-start upscale-engine-w-full">
      <div className="upscale-engine-mb-1 upscale-engine-text-sm upscale-engine-font-bold upscale-engine-ml-3">{forwardItem.text}</div>
      <div className="upscale-engine-rounded-md upscale-engine-w-full upscale-engine-border">
        {
          forwardItem.flow_master?.wikipedias?.map((item) => {
            return (
              <div key={item.id} className={'upscale-engine-w-full'}>
                <div onClick={() => {
                  if(activation.includes(item.id)) {
                    setActivation(activation.filter((i) => i !== item.id))
                  } else if (!activation.includes(item.id)) {
                    setActivation([...activation, item.id])
                  }
                }} className={'upscale-engine-cursor-pointer upscale-engine-bg-gray-100 upscale-engine-px-3 upscale-engine-py-2 upscale-engine-font-medium upscale-engine-text-sm upscale-engine-flex upscale-engine-items-center upscale-engine-justify-between upscale-engine-w-full'}>
                  <div>{item.title}</div>
                  <button className="upscale-engine-ml-2">
                    {
                      !activation.includes(item.id) && 
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4"
                      onClick={() => setActivation([...activation, item.id])}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                      </svg>
                    }
                    {
                      activation.includes(item.id) &&
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-4 upscale-engine-h-4"
                      onClick={() => setActivation(activation.filter((i) => i !== item.id))}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 15.75l7.5-7.5 7.5 7.5" />
                      </svg>
                    }
                  </button>
                </div>
                {
                  activation.includes(item.id) &&
                  <div className={'upscale-engine-text-sm upscale-engine-px-3 upscale-engine-py-4'}>{item.text}</div>
                }
              </div>
            )
          })
        }
      </div>
    </div>
  )
}