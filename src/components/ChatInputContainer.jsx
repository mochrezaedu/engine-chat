import {useEffect} from 'react'
import { useAtom } from "jotai"
import { inputChoice as derivedInputChoice } from '../services/stores'
import { inputDate as derivedInputDate } from '../services/stores'
import InputDate from './InputDate'
import InputChoice from './InputChoice'

export default function ChatInputContainer() {
  const [inputDate,setInputDate] = useAtom(derivedInputDate)
  const [inputChoice,setInputChoice] = useAtom(derivedInputChoice)

  return (
    <div>
      {
        inputDate.show &&
        <InputDate />
      }
      {
        inputChoice.show &&
        <InputChoice />
      }
      <InputChoice options={['test','test2']} />

    </div>
  )
}