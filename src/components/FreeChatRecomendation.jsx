import axios from "axios"
import { useAtom } from "jotai"
import { useMemo, useState } from "react"
import { useEffect } from "react"
import { useNextManager, useNlpSample, useRecommendation, useTalking } from "../services/hooks"
import { chatRecommendation, conversations } from "../services/stores"
import InputBase from "./InputBase"

export default function FreeChatRecomendation() {
  const recommendationState = useRecommendation()
  const recommendations = useRecommendation().getChatRecommendations()
  const nextGroup = useNextManager().nextGroup
  const [search,setSearch] = useState('')
  const _talk = useTalking().send
  const recommendation = useRecommendation()
  const [searching,setSearching] = useState(false)
  const nlpSample = useNlpSample()
  const [_conversations] = useAtom(conversations)
  const lastConversation = useMemo(() => _conversations[_conversations.length-1], [_conversations])


  const _handleClick = (args) => {
    nextGroup(args.id)
    nlpSample.setCorrection(args.name);
    recommendationState.resetChatRecommendation()
  }
  const _handleChangeSearch = (args) => {
    setSearch(args.target.value)
  }
  const _handleSubmit = (args) => {
    
  }
  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if(search && search?.length > 0) {
        setSearching(true)
        _talk(search).then((_r) => {
          recommendation.setChatRecommendations(_r.data)
        }).finally(() => {
          setSearching(false)
        })
      }
    }, 500)

    return () => clearTimeout(delayDebounceFn)
  }, [search])

  useEffect(() => {
    if(recommendations.length == 1) {
      nlpSample.setInput(lastConversation?.text)
      nlpSample.setCorrection(lastConversation?.text)
      nlpSample.setSuggestions(recommendations)
      _handleClick(recommendations.first())
    }
    nlpSample.setSuggestions(recommendations)
  }, [])

  return (
    <div className="upscale-engine-border upscale-engine-rounded upscale-engine-py-1">
      <div className="upscale-engine-font-semibold upscale-engine-px-2 upscale-engine-flex upscale-engine-items-center">
        <div className="upscale-engine-flex-grow upscale-engine-mr-4">Apakah maksud anda:</div>
        <button onClick={() => alert('Close widget.')} className='upscale-engine-bg-gray-200 upscale-engine-rounded-full upscale-engine-w-5 upscale-engine-h-5 upscale-engine-flex upscale-engine-justify-center upscale-engine-items-center'>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="upscale-engine-w-3 upscale-engine-h-3">
            <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 19.5l15-15m-15 0l15 15" />
          </svg>
        </button> 
      </div>
      {
        searching &&
        <div className="upscale-engine-text-xs upscale-engine-text-gray-500 upscale-engine-px-2">Sedang Mencari</div>
      }
      {
        Boolean(recommendations.length == 0) &&
        <div className="upscale-engine-px-2 upscale-engine-my-1 upscale-engine-text-sm"><i>Opps, No result</i></div>
      }
      {
        Boolean(recommendations.length > 0) &&
        <ul className="upscale-engine-list-disc upscale-engine-list-inside upscale-engine-my-1 upscale-engine-px-2">
          {
            recommendations.map((item) => (
              <li key={item.id} className="upscale-engine-text-indigo-700 upscale-engine-underline upscale-engine-list-disc upscale-engine-cursor-pointer" onClick={() => _handleClick(item)}>{item.name}</li>
            ))
          }
        </ul>
      }
      <div className="upscale-engine-border-t upscale-engine-border-gray-200 upscale-engine-mt-2 upscale-engine-py-1 upscale-engine-px-2">
        <InputBase 
        className={''}
        mode={'manual'} 
        value={search} 
        onSubmit={_handleSubmit} 
        placeholder={'Search'}
        onChange={_handleChangeSearch} 
        disableButton={true} />
      </div>
    </div>
  )
}