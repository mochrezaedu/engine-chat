import { atom, useAtom } from "jotai";

export const assistant = atom({name: 'PA', long_name: 'Phantom Assasin'})
export const user = atom(null)
export const userToken = atom(null)
export const anonymous = atom(true)
export const env = atom(import.meta.env)
export const flowGroups = atom([])
export const flows = atom([])

// CONVERSATIONS
export const conversations = atom([])
export const appendConversations = (props) => {
  const {state,setState,payload} = props
  const conversationInserted = state.find((item) => item.id === payload.id)
  if(!conversationInserted) {
    setState([...state, payload])
  }
}
export const pendingConversationIds = atom([])
// END CONVERSATIONS
// EDIT CONVERSATIONS
export const initialEditConversation = {
  show: false,
  item: null
}
export const editConversation = atom(initialEditConversation)
export const appendEditConversations = (props) => {
  const {state,setState,payload} = props
  const editConversationInserted = state.find((item) => item.id === payload.id)
  if(!editConversationInserted) {
    setState([...state, payload])
  }
}
export const pendingEditConversationIds = atom([])
// END EDIT CONVERSATIONS

export const lastConversation = atom(null)

// INPUT
export const initialInputBase = {
  type: 'text',
  label: '',
  value: null,
  status: 'disabled',
  show: true,
  placeholder: 'Tulis Chat'
}
export const inputBase = atom(initialInputBase)

export const initialInputDate = {
  type: 'date',
  label: '',
  value: null,
  status: 'disabled',
  show: false
}
export const inputDate = atom(initialInputDate)

export const initalInputChoice = {
  options: [],
  label: '',
  show: false
}
export const inputChoice = atom(initalInputChoice)

export const initalInputSelect = {
  resources: [],
  selected: [],
  filteredResources: [],
  type: 'basic',
  attributeLabel: 'skill_name',
  attributeId: 'skill_id',
  inputLabel: 'Pilih skill yang kamu kuasai.',
  isLoadingInputResources: false,
  open: false,
  input: {
    type: 'text',
    label: '',
    value: null,
    status: 'disabled',
    show: true,
    placeholder: 'Search'
  },
  show: false
}
export const inputSelect = atom(initalInputSelect)
// END INPUT 

export const initialChatRecommendation = {
  chats: [],
  show: false
}
export const chatRecommendation = atom(initialChatRecommendation)

// LOADERS
export const isTyping = atom(false)
export const isSendInput = atom(false)
// END LOADERS

export const busy = atom(true)

export const initialNlpSample = {
  input: '',
  correction: '',
  suggestions: []
}
export const nlpSample = atom(initialNlpSample)

export const initialCsSession = {
  status: null, // connecting, connected, ended
  cs: null, // CS Object
}
export const csSession = atom(initialCsSession)

