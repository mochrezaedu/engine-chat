import axios from "axios"
import { useAtom } from "jotai"
import { useEffect, useMemo } from "react"
import { 
  assistant,
  initalInputChoice,
  initalInputSelect,
  initialInputBase,
  initialInputDate,
  inputBase as derivedInputBase,
  inputChoice as derivedInputChoice,
  inputDate as derivedInputDate,
  inputSelect as derivedInputSelect,
  user,
  editConversation as derivedEditConversation,
  isTyping as derivedIsTyping,
  conversations as derivedConversations,
  appendConversations,
  busy,
  chatRecommendation,
  initialNlpSample,
  nlpSample,
  env,
  anonymous,
  userToken,
  csSession,
  initialCsSession
} from "./stores"

export const useStorage = () => {
  const [_env] = useAtom(env)
  const [,setUser] = useAtom(user)
  const [,setToken] = useAtom(userToken)
  const [,setAnonymous] = useAtom(anonymous)
  const prefix = _env.VITE_APP_PREFIX
  return {
    init: () => {
      const existingData = JSON.parse(localStorage.getItem(prefix))
      let data = {}
      if(existingData) {
        data = {...existingData}
        setUser(data.user)
        setToken(data.token)
        setAnonymous(data.anonymous)
      } else {
        data = {'token': null,'anonymous': false, 'user': null}
      }
      localStorage.setItem(prefix, JSON.stringify(data))
    },
    set: (key,value) => {
      const existingData = JSON.parse(localStorage.getItem(prefix))
      let data = {}
      if(existingData) {
        data = {...existingData}
      }
      data[key] = value
      localStorage.setItem(prefix, JSON.stringify(data))
    },
    get: (key) => {
      const data = JSON.parse(localStorage.getItem(prefix))
      return (data ? data[key]: null)
    }
  }
}

export const useInputManager = () => {
  const [,setInput] = useAtom(derivedInputBase)  
  const [,setInputChoice] = useAtom(derivedInputChoice)  
  const [,setInputDate] = useAtom(derivedInputDate)  
  const [,setInputSelect] = useAtom(derivedInputSelect)
  
  const actions = {
    resetInputs: () => {
      setInput(initialInputBase)
      setInputChoice(initalInputChoice)
      setInputDate(initialInputDate)
      setInputSelect(initalInputSelect)
    }
  }
  return {...actions}
}

export const useAttribute = () => {
  const [_u] = useAtom(user)
  const [_a] = useAtom(assistant)
  const attributes = {
    ":siang": 'Selamat siang',
    ":accountName": _u?.name?.split(' ').shift(),
    ":assistantName": _a?.name
  }
  return {
    attributeReplacer: (args) => {
      var text = args
      Object.keys(attributes).map((item) => {
        text = text?.replace(item, attributes[item])
      })
      return text
    }
  }
}

export const useConversation = () => {
  const [editConversation, setEditConversation] = useAtom(derivedEditConversation)
  return {
    resetEditConversation: () => {}
  }
}

export const useNextManager = () => {
  const [conversations, setConversations] = useAtom(derivedConversations)
  const lastConversationId = useMemo(() => conversations[conversations.length-1]?.id, [conversations])
  const lastConversation = useMemo(() => conversations[conversations.length-1], [conversations])
  const [,setIsTyping] = useAtom(derivedIsTyping)
  const [,setBusy] = useAtom(busy)

  const _handleTyping = (args) => {
    if(args?.flow) {
      if(args.type == 'sender') {
        setIsTyping(true)
        return true
      }
      if(args.flow?.next) {
        setIsTyping(true)
        if(['text','wiki_dropdown', 'text_image','text_image_editor_list'].includes(args.flow.type)) {
          return true
        }
      }
    }
    return false
  }

  return {
    automaticNext: () => {
      const typing = _handleTyping(lastConversation)
      axios.post('/conversation/get_next', {
        conversation_id: lastConversationId
      }).then((_resp) => {
        if(Boolean(_resp.data.flow)) {
          setBusy(true)
          axios.post('/conversation/store', {
            "flow": {id: _resp.data.flow.id}, "next": _resp.data.next, "type": "receiver", "text": _resp.data.flow.response
          }).then((_res) => {
            appendConversations({
              payload: _res.data,
              state: conversations,
              setState: setConversations
            })
          }).catch((_err) => {
            alert('Error while store Conversation')
            console.log(_err)
          })
        } else {
          setBusy(false)
        }
      }).catch((_err) => {
        console.log(_err);
        
      }).finally(() => {
        if(typing) {
          setTimeout(() => {
            setIsTyping(false)
          }, 500)
        }
      })
    },
    nextGroup: (id) => {
      const typing = _handleTyping(lastConversation)
      axios.post('/conversation/get_next_group', {
        g_id: id
      }).then((_resp) => {
        if(Boolean(_resp.data.flow)) {
          setBusy(true)
          axios.post('/conversation/store', {
            "flow": {id: _resp.data.flow.id}, "next": _resp.data.next, "type": "receiver", "text": _resp.data.flow.response
          }).then((_res) => {
            appendConversations({
              payload: _res.data,
              state: conversations,
              setState: setConversations
            })
          }).catch((_err) => {
            alert('Error while store Conversation')
            console.log(_err)
          })
        } else {
          setBusy(false)
        }
      }).catch((_err) => {
        console.log(_err);
      }).finally(() => {
        if(typing) {
          setTimeout(() => {
            setIsTyping(false)
          }, 5000)
        }
      })
    }
  }
}

export const useTalking = () => {
  const [conversations, setConversations] = useAtom(derivedConversations)
  const lastConversationId = useMemo(() => conversations[conversations.length-1]?.id, [conversations])
  const lastConversation = useMemo(() => conversations[conversations.length-1], [conversations])

  return {
    send: (text) => {
      return axios.post('/conversation/bot', {text: text || lastConversation.text})
    }
  }
}

export const useRecommendation = () => {
  const [_cr,set_cr] = useAtom(chatRecommendation)

  return {
    resetChatRecommendation: () => {
      set_cr({..._cr, show: false, chats: []})
    },
    setChatRecommendations: (items) => {
      const chatState = {
        chats: items,
        show: true
      }
      set_cr({..._cr, ...chatState})
    },
    getChatRecommendations: () => _cr.chats,
    show: _cr.show,
    data: _cr.chats
  }
}

export const useCsSession = () => {
  const [_cs, setCs] = useAtom(csSession)

  return {
    resetCsSession: () => {
      setCs(initialCsSession)
    },
    set: (cs, status) => {
      setCs({cs,status})
    },
    cs: _cs,
    isSearching: () => _cs.status == 'searching',
    isConnecting: () => _cs.status == 'connecting',
    isConnected: () => _cs.status == 'connected',
    isIdle: () => (_cs.status == null || _cs.status == 'idle')
  }
}

export const useNlpSample = () => {
  const [_nlpSample,_setNlpSample] = useAtom(nlpSample) 

  const _handleSendSample = () => {
    axios.post('nlp/store_user_correction',_nlpSample).finally(() => {
      _setNlpSample(initialNlpSample)
    })
  }

  useEffect(() => {
    if(_nlpSample.correction) {
      _handleSendSample()
    }
  }, [_nlpSample.correction])

  return {
    setInput: (args) => {
      _setNlpSample({..._nlpSample, input: args})
    },
    setCorrection: (args) => {
      _setNlpSample({..._nlpSample, correction: args})
    },
    setSuggestions: (args) => {
      _setNlpSample({..._nlpSample, suggestions: args})
    },
    sendSample: _handleSendSample
  }
}