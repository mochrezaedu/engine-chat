import axios from 'axios';

axios.defaults.baseURL = `${import.meta.env.VITE_API_URL}/api`;
// axios.defaults.headers.common['Authorization'] = 'Bearer 5e1031cc-3bd3-4aed-a80b-43f0e0b3380e';