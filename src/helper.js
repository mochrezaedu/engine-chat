export const timeSince = (date) => {
  var seconds = Math.floor((new Date() - Date.parse(date)) / 1000);

  var interval = seconds / 31536000;

  if (interval > 1) {
    return Math.floor(interval) + " tahun";
  }
  interval = seconds / 2592000;
  if (interval > 1) {
    return Math.floor(interval) + " bulan";
  }
  interval = seconds / 86400;
  if (interval > 1) {
    return Math.floor(interval) + " hari";
  }
  interval = seconds / 3600;
  if (interval > 1) {
    return Math.floor(interval) + " jam";
  }
  interval = seconds / 60;
  if (interval > 1) {
    return Math.floor(interval) + " menit";
  }
  return Math.floor(seconds) + " detik";
}

export const objectToCamelCase = (obj) => {
  let rtn = obj
  if(!rtn) {
    return rtn
  } else if (typeof (obj) === 'object' && obj !== null) {
    if (obj instanceof Array) {
      rtn = obj.map(toCamelCase)
    } else {
      rtn = {}
      for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
          const newKey = key.replace(/(_\w)/g, k => k[1].toUpperCase())
          rtn[newKey] = obj[key]
        }
      }
    }
  }
  
  return rtn
}